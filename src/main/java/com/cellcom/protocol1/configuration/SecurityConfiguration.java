package com.cellcom.protocol1.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private DataSource dataSource;

    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery).
                dataSource(dataSource).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                // URLs matching for access rights
                .antMatchers("/sendToken").permitAll()
                .antMatchers("/password").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/verify/{email}").permitAll()
                .antMatchers("/recover-password/{email}/{token}").permitAll()
                .antMatchers("/assign-role").access("hasRole('ROLE_SUPER_ADMIN')")
                .antMatchers("/users").access("hasRole('ROLE_SUPER_ADMIN')")
                .antMatchers("/report").access("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_SUPPORT_ADMIN')")
                .antMatchers("/bankAccountData").access("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_SUPPORT_ADMIN')")
                .antMatchers("/merchants").access("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_SUPPORT_ADMIN')")
                .antMatchers("/approve-count").access("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_SUPPORT_ADMIN')")
                .antMatchers("/flag-count").access("hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_SUPPORT_ADMIN')")
                .anyRequest().authenticated()
                .and()
                // form login
                .csrf().disable().formLogin()
                .loginPage("/login")
                .failureUrl("/login?error=true")
                .defaultSuccessUrl("/welcome")
                .usernameParameter("email")
                .passwordParameter("password")
                .and()
                // logout
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login").and()
                .exceptionHandling()
                .accessDeniedPage("/access-denied");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }
}
