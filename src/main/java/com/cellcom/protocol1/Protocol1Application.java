package com.cellcom.protocol1;

import com.cellcom.protocol1.service.SendEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class Protocol1Application {

	private static final Logger logger = LogManager.getLogger(Protocol1Application.class.getName());

	public static void main(String[] args) {

		SpringApplication.run(Protocol1Application.class, args);
	}

	@Component
	class DemoCommandlineRunner implements CommandLineRunner {

		@Autowired
		private SendEmail sendEmail;

		@Override
		public void run(String... args) throws Exception {
		}
	}
}
