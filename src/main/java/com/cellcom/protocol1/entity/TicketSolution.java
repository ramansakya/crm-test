package com.cellcom.protocol1.entity;

import javax.persistence.*;

@Entity
@Table( name = "ticket_solution")
public class TicketSolution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "solution_value")
    private String solution;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ticket_id", nullable = false)
    private Ticket ticket;

    public TicketSolution(){}

    public TicketSolution(Long id, String solution, Ticket ticket) {
        this.id = id;
        this.solution = solution;
        this.ticket = ticket;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
