package com.cellcom.protocol1.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bank")
public class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "test_ip")
    private String testIP;

    @Column(name = "test_port")
    private String testPort;

    @Column(name = "live_ip")
    private String liveIP;

    @Column(name = "live_port")
    private String livePort;

    @Column(name = "cbs_test_ip")
    private String cbsTestIP;

    @Column(name = "cbs_test_port")
    private String cbsTestPort;

    @Column(name = "cbs_live_ip")
    private String cbsLiveIP;

    @Column(name = "cbs_live_port")
    private String cbsLivePort;

    @Column(name = "follow_up")
    private String followUp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bank")
    private Set<PointOfContact> pointOfContacts = new HashSet<PointOfContact>(0);

    @Column(name = "vpn_parameters")
    private String vpnParameters;

    @Column(name = "vpn_done")
    private String vpn;

    @Column(name = "o_testing")
    private String isoTesting;

    @Column(name = "account_link_method")
    private String accountLinkMethod;

    @Column(name = "account_link")
    private String accountLink;

    @Column(name = "testing")
    private String testing;

    @Column(name = "management_team_approval")
    private String managementTeamApproval;

    @Column(name = "live")
    private String live;

    public Bank() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestIP() {
        return testIP;
    }

    public void setTestIP(String testIP) {
        this.testIP = testIP;
    }

    public String getTestPort() {
        return testPort;
    }

    public void setTestPort(String testPort) {
        this.testPort = testPort;
    }

    public String getLiveIP() {
        return liveIP;
    }

    public void setLiveIP(String liveIP) {
        this.liveIP = liveIP;
    }

    public String getLivePort() {
        return livePort;
    }

    public void setLivePort(String livePort) {
        this.livePort = livePort;
    }

    public String getCbsTestIP() {
        return cbsTestIP;
    }

    public void setCbsTestIP(String cbsTestIP) {
        this.cbsTestIP = cbsTestIP;
    }

    public String getCbsTestPort() {
        return cbsTestPort;
    }

    public void setCbsTestPort(String cbsTestPort) {
        this.cbsTestPort = cbsTestPort;
    }

    public String getCbsLiveIP() {
        return cbsLiveIP;
    }

    public void setCbsLiveIP(String cbsLiveIP) {
        this.cbsLiveIP = cbsLiveIP;
    }

    public String getCbsLivePort() {
        return cbsLivePort;
    }

    public void setCbsLivePort(String cbsLivePort) {
        this.cbsLivePort = cbsLivePort;
    }

    public String getFollowUp() {
        return followUp;
    }

    public void setFollowUp(String followUp) {
        this.followUp = followUp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<PointOfContact> getPointOfContacts() {
        return pointOfContacts;
    }

    public void setPointOfContacts(Set<PointOfContact> pointOfContacts) {
        this.pointOfContacts = pointOfContacts;
    }

    public String getVpnParameters() {
        return vpnParameters;
    }

    public void setVpnParameters(String vpnParameters) {
        this.vpnParameters = vpnParameters;
    }

    public String getVpn() {
        return vpn;
    }

    public void setVpn(String vpn) {
        this.vpn = vpn;
    }

    public String getIsoTesting() {
        return isoTesting;
    }

    public void setIsoTesting(String isoTesting) {
        this.isoTesting = isoTesting;
    }

    public String getAccountLinkMethod() {
        return accountLinkMethod;
    }

    public void setAccountLinkMethod(String accountLinkMethod) {
        this.accountLinkMethod = accountLinkMethod;
    }

    public String getAccountLink() {
        return accountLink;
    }

    public void setAccountLink(String accountLink) {
        this.accountLink = accountLink;
    }

    public String getTesting() {
        return testing;
    }

    public void setTesting(String testing) {
        this.testing = testing;
    }

    public String getManagementTeamApproval() {
        return managementTeamApproval;
    }

    public void setManagementTeamApproval(String managementTeamApproval) {
        this.managementTeamApproval = managementTeamApproval;
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }
}
