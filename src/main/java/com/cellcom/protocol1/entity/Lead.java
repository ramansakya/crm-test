package com.cellcom.protocol1.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.sql.Blob;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "lead")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "company_address")
    private String companyAddress;

    @Column(name = "company_number")
    private String companyNumber;

    @Column(name = "company_email")
    private String companyEmail;

    @Length(max = 10, message = "Mobile number must be 10 digits")
    @Pattern(regexp = "^98[0-9]{8}")
    @Column(name = "registered_mobile")
    private String registeredMobile;

    @Column(name = "registration_no")
    private String registrationNumber;

    @Column(name = "bank")
    private String bank;

    @Column(name = "bank_account")
    private String bankAccount;

    @Column(name = "PAN_VAT")
    private String PanVat;

    @Column(name = "representative_name")
    private String representativeName;

    @Length(max = 10, message = "Mobile number must be 10 digits")
    @Pattern(regexp = "^98[0-9]{8}")
    @Column(name = "representative_number")
    private String representativeNumber;

    @Email(message = "Email is not valid")
    @Column(name = "representative_email", unique = true)
    private String representativeEmail;

    @Column(name = "registration_doc")
    private Blob registrationDoc;

    @Column(name = "PAN_VAT_doc")
    private Blob PanVatDoc;

    @Column(name = "board_minute_doc")
    private Blob boardMinuteDoc;

    @Column(name = "agreement_doc")
    private Blob agreementDoc;

    @Column(name = "date")
    private Date date;

    @Column(name = "status")
    private String status;

    @Column(name = "remarks")
    private String remarks;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lead")
    private Set<Operator> operators = new HashSet<Operator>(0);

    public Lead() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getRegisteredMobile() {
        return registeredMobile;
    }

    public void setRegisteredMobile(String registeredMobile) {
        this.registeredMobile = registeredMobile;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getPanVat() {
        return PanVat;
    }

    public void setPanVat(String panVat) {
        PanVat = panVat;
    }

    public String getRepresentativeName() {
        return representativeName;
    }

    public void setRepresentativeName(String representativeName) {
        this.representativeName = representativeName;
    }

    public String getRepresentativeNumber() {
        return representativeNumber;
    }

    public void setRepresentativeNumber(String representativeNumber) {
        this.representativeNumber = representativeNumber;
    }

    public String getRepresentativeEmail() {
        return representativeEmail;
    }

    public void setRepresentativeEmail(String representativeEmail) {
        this.representativeEmail = representativeEmail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Blob getRegistrationDoc() {
        return registrationDoc;
    }

    public void setRegistrationDoc(Blob registrationDoc) {
        this.registrationDoc = registrationDoc;
    }

    public Blob getPanVatDoc() {
        return PanVatDoc;
    }

    public void setPanVatDoc(Blob panVatDoc) {
        PanVatDoc = panVatDoc;
    }

    public Blob getBoardMinuteDoc() {
        return boardMinuteDoc;
    }

    public void setBoardMinuteDoc(Blob boardMinuteDoc) {
        this.boardMinuteDoc = boardMinuteDoc;
    }

    public Blob getAgreementDoc() {
        return agreementDoc;
    }

    public void setAgreementDoc(Blob agreementDoc) {
        this.agreementDoc = agreementDoc;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Operator> getOperators() {
        return operators;
    }

    public void setOperators(Set<Operator> operators) {
        this.operators = operators;
    }

}
