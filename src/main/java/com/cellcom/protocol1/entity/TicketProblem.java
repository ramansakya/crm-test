package com.cellcom.protocol1.entity;

import javax.persistence.*;

@Entity
@Table(name = "ticket_problem")
public class TicketProblem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "problem_value")
    private String problem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ticket_id", nullable = false)
    private Ticket ticket;

    public TicketProblem(){}

    public TicketProblem(Long id, String problem, Ticket ticket) {
        this.id = id;
        this.problem = problem;
        this.ticket = ticket;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
