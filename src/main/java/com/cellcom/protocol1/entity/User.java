package com.cellcom.protocol1.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name = "user_id")
    private Long id;

    @Column(name = "name")
    @NotNull(message = "Enter your full name")
    private String name;

    @Column(name = "email", unique = true)
    @NotNull(message= "Email is compulsory")
    @Email(message = "Email is not valid")
    private String email;

    @Column(name = "password")
    private String password;

    @Transient
    private String cpassword;

    @Column(name = "token")
    private String token;

    @Column(name = "mobile")
    @NotNull(message = "Mobile number is compulsory")
    @Length(max = 10, message = "Mobile number must be 10 digits")
    @Pattern(regexp = "^98[0-9]{8}")
    private String mobile;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "first_login")
    private Boolean firstLogin;

    @Column(name = "status")
    private Boolean status;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Member> members = new HashSet<Member>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Denial> denials = new HashSet<Denial>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Flag> flags = new HashSet<Flag>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Lead> leads = new HashSet<Lead>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Bank> banks = new HashSet<Bank>(0);

    public User() {
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getCpassword() { return cpassword; }

    public void setCpassword(String cpassword) { this.cpassword = cpassword; }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }

    public Set<Denial> getDenials() {
        return denials;
    }

    public void setDenials(Set<Denial> denials) {
        this.denials = denials;
    }

    public Set<Lead> getLeads() {
        return leads;
    }

    public void setLeads(Set<Lead> leads) {
        this.leads = leads;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Set<Bank> getBanks() {
        return banks;
    }

    public void setBanks(Set<Bank> banks) {
        this.banks = banks;
    }
}
