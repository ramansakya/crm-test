package com.cellcom.protocol1.service;

import com.cellpay.v2.cellpaydatas.entity.*;
import com.cellpay.v2.cellpaydatas.serviceImpl.DashboardDataServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DashBoardDataServiceImpl {

    Logger logger = LogManager.getLogger(CustomerDetailServiceImpl.class);

    public DashboardData showData(){
        DashboardData dashboardData = DashboardDataServiceImpl.getDashData();
        logger.info("Total Registration: " + dashboardData.getTotalRegistration());
        logger.info("FullKYC: " + dashboardData.getFullKYC());
        logger.info("SemiKYC: " + dashboardData.getSemiKYC());
        logger.info("Bank: " + dashboardData.getTotalBank());
        logger.info("Total Merchant: " + dashboardData.getTotalMerchant());
        logger.info("Semi Merchant: " + dashboardData.getSemiMerchant());
        logger.info("Total Customer: " + dashboardData.getTotalCustomer());
        logger.info("Semi Customer: " + dashboardData.getSemiCustomer());
        logger.info("Total Agent: " + dashboardData.getTotalAgent());
        logger.info("Semi Agent: " + dashboardData.getSemiAgent());
        logger.info("Monthly Registration: " + dashboardData.getMonthlyRegistration());
        logger.info("Weekly Registration: " + dashboardData.getWeeklyRegistration());
        return dashboardData;
    }

    public List<BankAccountData> showBankAccountData(){
        List<BankAccountData> bankAccountDataArrayList = DashboardDataServiceImpl.getBankAccountUserList();
        for(BankAccountData bankAccountData: bankAccountDataArrayList){
            logger.info(bankAccountData.getMemberName());
            logger.info(bankAccountData.getMobileNumber());
            logger.info(bankAccountData.getBankName());
            logger.info(bankAccountData.getAccountNumber());
            logger.info(bankAccountData.getSignupDate());
            logger.info(bankAccountData.getBankAddedDate());
            logger.info(bankAccountData.getGroupName());
        }
        return bankAccountDataArrayList;
    }

    public List<NoBankAccountData> showNoBankAccountData(){
        List<NoBankAccountData> noBankAccountDataList = DashboardDataServiceImpl.getNoBankAccountLinkData();
        for(NoBankAccountData noBankAccountData: noBankAccountDataList){
            logger.info(noBankAccountData.getMemberName());
            logger.info(noBankAccountData.getMobileNumber());
            logger.info(noBankAccountData.getGroupName());
            logger.info(noBankAccountData.getSignupDate());
        }
        return noBankAccountDataList;
    }

    public List<Bank> showBanks(){
        List<Bank> banks = DashboardDataServiceImpl.getBanklist();
        for(Bank bank: banks){
            logger.info(bank.getBankName());
            logger.info(bank.getCreationDate());
            logger.info(bank.getIpAddr());
            logger.info(bank.getPort());
            logger.info(bank.getPackagerName());
        }
        return banks;
    }

    public List<MerchantData> showMerchants(){
        List<MerchantData> merchants = DashboardDataServiceImpl.getMerchantList();
        for(MerchantData merchantData: merchants){
            logger.info(merchantData.getMemberName());
            logger.info(merchantData.getMobileNumber());
            logger.info(merchantData.getCreationDate());
            logger.info(merchantData.getCompanyName());
        }
        return merchants;
    }
}
