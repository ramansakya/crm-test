package com.cellcom.protocol1.service;

import javax.mail.MessagingException;
import java.io.IOException;

public interface SendEmail {
    void sendEmail(String body, String to) throws MessagingException, IOException;
}
