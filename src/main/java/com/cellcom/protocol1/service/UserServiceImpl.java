package com.cellcom.protocol1.service;

import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class UserServiceImpl{

    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    UserRepository userRepository;

    public String getRandomString() {
        String randomString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * randomString.length());
            salt.append(randomString.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    public Date getCurrentDate(){
        Date todayDate = new Date();
        return todayDate;
    }

    public String getTodayDate(){
        Date todayDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String todayDateStr = dateFormat.format(todayDate);
        return todayDateStr;
    }

    public User saveUser(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        user.setCreationDate(getCurrentDate());
        user.setFirstLogin(false);
        return userRepository.save(user);
    }

    public boolean isUserAlreadyPresent(User user) {
        boolean isUserAlreadyExists = false;
        User existingUser = userRepository.findByEmail(user.getEmail());
        if (existingUser != null) {
            isUserAlreadyExists = true;
        }
        return isUserAlreadyExists;
    }
}