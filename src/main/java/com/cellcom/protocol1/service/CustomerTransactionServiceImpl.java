package com.cellcom.protocol1.service;

import com.cellpay.v2.cellpaydatas.entity.CustomerTransactionHistory;
import com.cellpay.v2.cellpaydatas.entity.FailureTransaction;
import com.cellpay.v2.cellpaydatas.serviceImpl.CustomerFailureTransServiceImpl;
import com.cellpay.v2.cellpaydatas.serviceImpl.CustomerTransactionDetailsImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomerTransactionServiceImpl {

    Logger logger = LogManager.getLogger(CustomerTransactionServiceImpl.class);

    public ArrayList<CustomerTransactionHistory> showTransactions(String mobileNumber, String startDate, String endDate){
        ArrayList<CustomerTransactionHistory> customerTransactionsHistory = CustomerTransactionDetailsImpl.getCustomerTransactionHistory(mobileNumber, startDate, endDate);
        for (CustomerTransactionHistory customerTransactionHistory: customerTransactionsHistory){
            logger.info(customerTransactionHistory.getTransId());
            logger.info(customerTransactionHistory.getTransDate());
            logger.info(customerTransactionHistory.getTransType());
            logger.info(customerTransactionHistory.getTransFromTo());
            logger.info(customerTransactionHistory.getTransAmount());
            logger.info(customerTransactionHistory.getTransFees());
            logger.info(customerTransactionHistory.getTransCommission());
            logger.info(customerTransactionHistory.getTransDrCr());
            logger.info(customerTransactionHistory.getTransBank());
            logger.info(customerTransactionHistory.getTransRemarks());
        }
        return customerTransactionsHistory;
    }

    public ArrayList<FailureTransaction> failTransactions(String mobileNumber, String startDate, String endDate) {
        ArrayList<FailureTransaction> failureTransactions = CustomerFailureTransServiceImpl.getFailureTransactionList(mobileNumber, startDate, endDate);
        for (FailureTransaction failureTransaction : failureTransactions) {
            logger.info(failureTransaction.getTransactionId());
            logger.info(failureTransaction.getSubTransactionId());
            logger.info(failureTransaction.getBankTransactionId());
            logger.info(failureTransaction.getBankTransacted());
            logger.info(failureTransaction.getIsSettlement());
            logger.info(failureTransaction.getIsReversal());
            logger.info(failureTransaction.getFromMobileNo());
            logger.info(failureTransaction.getToMobileNo());
            logger.info(failureTransaction.getTransactionAmount());
            logger.info(failureTransaction.getTransDateTime());
            logger.info(failureTransaction.getTransStatus());
            logger.info(failureTransaction.getBankResponseCode());
        }
        return failureTransactions;
    }
}
