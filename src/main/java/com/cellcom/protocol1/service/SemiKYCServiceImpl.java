package com.cellcom.protocol1.service;

import com.cellpay.v2.cellpaydatas.entity.SemiKYCUsers;
import com.cellpay.v2.cellpaydatas.serviceImpl.KYCInformationServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class SemiKYCServiceImpl {

    Logger logger = LogManager.getLogger(SemiKYCServiceImpl.class);

    public ArrayList<SemiKYCUsers> showList(){
        ArrayList<SemiKYCUsers> semiKYCUsers = KYCInformationServiceImpl.getSemiKYCUsersList();
        logger.info(semiKYCUsers);
        return semiKYCUsers;
    }
}
