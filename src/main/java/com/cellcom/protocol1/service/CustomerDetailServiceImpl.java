package com.cellcom.protocol1.service;

import com.cellpay.v2.cellpaydatas.entity.*;
import com.cellpay.v2.cellpaydatas.serviceImpl.CustomerDetailsServiceImpl;
import com.cellpay.v2.cellpaydatas.serviceImpl.DashboardDataServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerDetailServiceImpl {

    Logger logger = LogManager.getLogger(CustomerDetailServiceImpl.class);

    public CustomerDetails showCustomerDetails(String mobileNumber){
        CustomerDetails customerDetails = CustomerDetailsServiceImpl.getCustomerDetails(mobileNumber);
        logger.info(customerDetails.getEmail());
        logger.info(customerDetails.getName());
        logger.info(customerDetails.getMobileNumber());
        logger.info(customerDetails.getLoginName());
        return customerDetails;
    }

    public ArrayList<CustomerKYCDetails> showCustomerKYCDetails(String mobileNumber){
        ArrayList<CustomerKYCDetails> customerKYCDetails = CustomerDetailsServiceImpl.getCustomerKYCDetails(mobileNumber);
        for (CustomerKYCDetails customerKYC: customerKYCDetails) {
            logger.info(customerKYC.getDocType());
            logger.info(customerKYC.getDocValue());
        }
        return customerKYCDetails;
    }

    public CustomerStatus showCustomerStatus(String mobileNumber){
        CustomerStatus customerStatus = CustomerDetailsServiceImpl.getCustomerStatus(mobileNumber);
        logger.info(customerStatus.getGroupId());
        logger.info(customerStatus.getGroupName());
        logger.info(customerStatus.getCustomerStatus());
        return customerStatus;
    }

    public ArrayList<CustomerDocuments> showCustomerDocuments(String mobileNumber){
        ArrayList<CustomerDocuments> customerDocuments = CustomerDetailsServiceImpl.getCustomerDocuments(mobileNumber);
        for (CustomerDocuments customerDocument: customerDocuments){
            logger.info(customerDocument.getMemberId());
            logger.info(customerDocument.getDocName());
            logger.info(customerDocument.getDocType());
            logger.info(customerDocument.getDocImage());
        }
        return customerDocuments;
    }

    public ArrayList<AgentData> showAgents(){
        ArrayList<AgentData> agentDataArrayList = CustomerDetailsServiceImpl.getAgentsData();
        for (AgentData agentData: agentDataArrayList){
            logger.info(agentData.getMemberId());
            logger.info(agentData.getMemberName());
            logger.info(agentData.getMobileNumber());
            logger.info(agentData.getPanNumber());
            logger.info(agentData.getIdentityType());
            logger.info(agentData.getIdentityNumber());
            logger.info(agentData.getIdentityIssuedPlace());
            logger.info(agentData.getIdentityIssuedDate());
            logger.info(agentData.getAddress());
            logger.info(agentData.getWardNumber());
        }
        return agentDataArrayList;
    }

    public ArrayList<CustomerDataLatestModified> showData(){
        ArrayList<CustomerDataLatestModified> customerDataLatestModifieds = CustomerDetailsServiceImpl.getCustomerDetailsLatestModified();
        for(CustomerDataLatestModified customerDataLatestModified: customerDataLatestModifieds){
            logger.info(customerDataLatestModified.getCount());
            logger.info(customerDataLatestModified.getMemberId());
            logger.info(customerDataLatestModified.getMemberName());
            logger.info(customerDataLatestModified.getMobileNo());
            logger.info(customerDataLatestModified.getCreationDate());
            logger.info(customerDataLatestModified.getGroupName());
            logger.info(customerDataLatestModified.getToken());
            logger.info(customerDataLatestModified.getLastModifiedDate());
        }
        return customerDataLatestModifieds;
    }
}
