package com.cellcom.protocol1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Service
public class SendEmailImpl implements SendEmail {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String from;

    public void sendEmail(String body, String to) throws MessagingException, IOException {
        MimeMessage msg = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject("Testing Email");
        helper.setText("<div style='width:700px;height:500px;align:center; border-radius:10px; box-shadow:#cccccc 0 0 5px 2px'>" +
                "<header style='border-bottom:#193983 1px solid'>" +
                "<img align='center' src='https://cellcom.net.np/wp-content/uploads/2019/07/cellpay-546.png' style='display:block; width:130px;height:60px;margin:10px 300px !important;'></img>" +
                "</header>" +
                "<div background='https://app.cellpay.com.np/images/bg.jpg' style='border-bottom: #193983 1px solid'>" +
                body +
                "<br /><br /> Regards, <br /> CellPay Admin <br />" +
                "</div> <footer style='color:white;background-color: white;'> <p> " +
                "<table style='color: #193983;'>" +
                "<tbody>" +
                "<tr>" +
                "<td style='width:400px'><table>" +
                "<tbody padding-left=10px><tr>" +
                "<td style='color:#193983'>Email</td><td style='color:#193983'> - <a style='color:#193983' href='mailto:support@cellpay.com.np' target='_blank'>support@cellpay.com.np</a></td>" +
                "</tr>" +
                "<tr ><td >Toll Free</td><td>16600142555</td></tr>" +
                "<tr><td>Landline</td><td>+9774265361; +9774265275</td></tr>" +
                "</tbody></table></td>" +
                "</tr></tbody></table></p><div style='text-align:cecnter; color: black;'><h6><em>Copyright @copy; <span id=\"date\">, CellPay, All right reserved.</em></h6> </div></footer></div><script> n = new Date(); y=n.getFullYear(); document.getElementById(\"date\").innerHTML = y;</script></script>", true);
        javaMailSender.send(msg);
    }
}
