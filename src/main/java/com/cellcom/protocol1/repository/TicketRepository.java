package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Ticket;
import com.cellcom.protocol1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query("select s from Ticket s where s.user=?1")
    List<User> findByUserId(User user);

    @Query("select s from Ticket s  where s.assignedUser=?1")
    List<Ticket> findTicketsByUser(User user);
}
