package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Denial;
import com.cellcom.protocol1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DenialRepository extends JpaRepository<Denial, Long> {

    @Query("select d from Denial d group by d.user")
    List<Denial> getCount();

    Long countByUser(User user);

    @Query("select d from Denial d where date(d.date) >= date(?1) and date(d.date) <= date(?2) group by d.user")
    List<Denial> getDenialUsingDate(String startDate, String endDate);

    @Query("Select count(d) from Denial d where date(d.date) >= date(?1) and date(d.date) <= date(?2) and d.user=?3")
    Long countGroupByUser(String startDate, String endDate, User user);

    @Query("select d from Denial d where date(d.date) >= date(?1) and date(d.date) <= date(?2) and d.user=?3")
    List<Denial> getDenialUsingUserAndDate(String startDate, String endDate, User user);

    @Query("select d from Denial d where d.user=?1")
    List<Denial> findByUserId(User user);

    @Query("select count(d) from Denial d")
    Long countAll();
}
