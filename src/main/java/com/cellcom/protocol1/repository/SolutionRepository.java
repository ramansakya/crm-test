package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Problem;
import com.cellcom.protocol1.entity.Solution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SolutionRepository extends JpaRepository<Solution, Long> {

//    @Query("select s from Solution s where s.problem=?1")
//    List<Solution> findAllByProblem(Problem problem);

}
