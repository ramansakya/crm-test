package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    @Query("select s from UserProfile s where s.user=?1")
    UserProfile findByUserId(User user);
}
