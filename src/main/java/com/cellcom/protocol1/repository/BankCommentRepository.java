package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Bank;
import com.cellcom.protocol1.entity.BankComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankCommentRepository extends JpaRepository<BankComment, Long> {

    @Query("select b from BankComment b where b.bank=?1")
    List<BankComment> findAllByBank(Bank bank);
}
