package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Member;
import com.cellcom.protocol1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

    @Query("select m from Member m group by m.user")
    List<Member> getCount();

    Long countByUser(User user);

    @Query("select m from Member m where date(m.date) >= date(?1) and date(m.date) <= date(?2) group by m.user")
    List<Member> getMemberUsingDate(String startDate, String endDate);

    @Query("Select count(m) from Member m where date(m.date) >= date(?1) and date(m.date) <= date(?2) and m.user=?3")
    Long countGroupByUser(String startDate, String endDate, User user);

    @Query("select m from Member m where date(m.date) >= date(?1) and date(m.date) <= date(?2) and m.user=?3")
    List<Member> getMemberUsingUserAndDate(String startDate, String endDate, User user);

    @Query("select m from Member m where m.user=?1")
    List<Member> findByUserId(User user);

    @Query("select count(m) from Member m")
    Long countAll();

}
