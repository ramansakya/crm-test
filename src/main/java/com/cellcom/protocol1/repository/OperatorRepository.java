package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Lead;
import com.cellcom.protocol1.entity.Operator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperatorRepository extends JpaRepository<Operator, Long> {

    @Query("select o from Operator o where o.lead=?1")
    List<Operator> findAllByLead(Lead lead);
}
