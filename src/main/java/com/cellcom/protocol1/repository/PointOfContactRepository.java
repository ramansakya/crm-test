package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Bank;
import com.cellcom.protocol1.entity.PointOfContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PointOfContactRepository extends JpaRepository<PointOfContact, Long> {

    @Query("select p from PointOfContact p where p.bank=?1")
    List<PointOfContact> findAllByBank(Bank bank);
}
