package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Ticket;
import com.cellcom.protocol1.entity.TicketComments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketCommentsRepository extends JpaRepository<TicketComments, Long> {

    @Query("select s from TicketComments s where s.ticket=?1")
    List<TicketComments> findByTicketId(Ticket ticket);
}
