package com.cellcom.protocol1.repository;

import com.cellcom.protocol1.entity.Flag;
import com.cellcom.protocol1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlagRepository extends JpaRepository<Flag, Long> {

    @Query("select f from Flag f group by f.user")
    List<Flag> getCount();

    Long countByUser(User user);

    @Query("select f from Flag f where date(f.date) >= date(?1) and date(f.date) <= date(?2) group by f.user")
    List<Flag> getFlagUsingDate(String startDate, String endDate);

    @Query("Select count(f) from Flag f where date(f.date) >= date(?1) and date(f.date) <= date(?2) and f.user=?3")
    Long countGroupByUser(String startDate, String endDate, User user);

    @Query("select f from Flag f where date(f.date) >= date(?1) and date(f.date) <= date(?2) and f.user=?3")
    List<Flag> getFlagUsingUserAndDate(String startDate, String endDate, User user);

    @Query("select f from Flag f where f.user=?1")
    List<Flag> findByUserId(User user);

    @Query("select count(f) from Flag f")
    Long countAll();
}
