package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Denial;
import com.cellcom.protocol1.entity.Flag;
import com.cellcom.protocol1.entity.Member;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.DenialRepository;
import com.cellcom.protocol1.repository.FlagRepository;
import com.cellcom.protocol1.repository.MemberRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.CustomerDetailServiceImpl;
import com.cellpay.v2.cellpaydatas.entity.AgentData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ReportController {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private DenialRepository denialRepository;

    @Autowired
    private FlagRepository flagRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerDetailServiceImpl customerDetailServiceImpl;

    @GetMapping("/approve-count")
    public String approveCount(Model model){
        List<Member> memberList = memberRepository.getCount();
        List<Member> memberList1 = new ArrayList<>();
        for(Member member : memberList){
            Member member1 = new Member();
            Long count = memberRepository.countByUser(member.getUser());
            member1.setUser(member.getUser());
            member1.setDate(member.getDate());
            member1.setCount(count);
            memberList1.add(member1);
        }
        model.addAttribute("members", memberList1);
        return "approve-count";
    }

    @GetMapping("/denial-count")
    public String denialCount(Model model){
        List<Denial> denialList = denialRepository.getCount();
        List<Denial> denialList1 = new ArrayList<>();
        for(Denial denial : denialList){
            Denial denial1 = new Denial();
            Long count = denialRepository.countByUser(denial.getUser());
            denial1.setUser(denial.getUser());
            denial1.setCount(count);
            denialList1.add(denial1);
        }
        model.addAttribute("denials", denialList1);
        return "denial-count";
    }

    @GetMapping("/flag-count")
    public String flagCount(Model model){
        List<Flag> flagList = flagRepository.getCount();
        List<Flag> flagList1 = new ArrayList<>();
        for(Flag flag : flagList){
            Flag flag1 = new Flag();
            Long count = flagRepository.countByUser(flag.getUser());
            flag1.setUser(flag.getUser());
            flag1.setCount(count);
            flagList1.add(flag1);
        }
        model.addAttribute("flags", flagList1);
        return "flag-count";
    }

    @GetMapping("/approvedReport/{user_id}")
    public String approvedReport(@PathVariable String user_id, Model model){
        User user = userRepository.findById(Long.parseLong(user_id)).get();
        List<Member> members = memberRepository.findByUserId(user);
        model.addAttribute("user", user);
        model.addAttribute("members", members);
        return "approve-report";
    }

    @GetMapping("/denialReport/{user_id}")
    public String denialReport(@PathVariable String user_id, Model model){
        User user = userRepository.findById(Long.parseLong(user_id)).get();
        List<Denial> denials = denialRepository.findByUserId(user);
        model.addAttribute("user", user);
        model.addAttribute("denials", denials);
        return "denial-report";
    }

    @GetMapping("/flagReport/{user_id}")
    public String flagReport(@PathVariable String user_id, Model model){
        User user = userRepository.findById(Long.parseLong(user_id)).get();
        List<Flag> flags = flagRepository.findByUserId(user);
        model.addAttribute("user", user);
        model.addAttribute("flags", flags);
        return "flag-report";
    }

    @GetMapping("/approve-count/{user_id}/{startDate}/{endDate}")
    public String asd(@PathVariable String user_id, @PathVariable String startDate, @PathVariable String endDate, Model model){
        User user = userRepository.findById(Long.valueOf(user_id)).get();
        List<Member> members = memberRepository.getMemberUsingUserAndDate(startDate, endDate, user);
        model.addAttribute("user", user);
        model.addAttribute("members", members);
        return "approve-report";
    }

    @GetMapping("/denial-count/{user_id}/{startDate}/{endDate}")
    public String asdf(@PathVariable String user_id, @PathVariable String startDate, @PathVariable String endDate, Model model){
        User user = userRepository.findById(Long.valueOf(user_id)).get();
        List<Denial> denials = denialRepository.getDenialUsingUserAndDate(startDate, endDate, user);
        model.addAttribute("user", user);
        model.addAttribute("denials", denials);
        return "denial-report";
    }

    @GetMapping("/flag-count/{user_id}/{startDate}/{endDate}")
    public String asde(@PathVariable String user_id, @PathVariable String startDate, @PathVariable String endDate, Model model){
        User user = userRepository.findById(Long.valueOf(user_id)).get();
        List<Flag> flags = flagRepository.getFlagUsingUserAndDate(startDate, endDate, user);
        model.addAttribute("user", user);
        model.addAttribute("flags", flags);
        return "flag-report";
    }

    @PostMapping("/showApproveCount")
    public String showApproveCount(@RequestParam String startDate, @RequestParam String endDate, Model model){
        List<Member> members = memberRepository.getMemberUsingDate(startDate, endDate);
        List<Member> memberList2 = new ArrayList<>();
        for(Member member2 : members) {
            Member member3 = new Member();
            Long count1 = memberRepository.countGroupByUser(startDate, endDate, member2.getUser());
            member3.setUser(member2.getUser());
            member3.setCount(count1);
            memberList2.add(member3);
        }
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("members", memberList2);
        return "approve-count";
    }

    @PostMapping("/showDenialCount")
    public String showDenialCount(@RequestParam String startDate, @RequestParam String endDate, Model model){
        List<Denial> denials = denialRepository.getDenialUsingDate(startDate, endDate);
        List<Denial> denialList2 = new ArrayList<>();
        for(Denial denial2 : denials) {
            Denial denial3 = new Denial();
            Long count1 = denialRepository.countGroupByUser(startDate, endDate, denial2.getUser());
            denial3.setUser(denial2.getUser());
            denial3.setCount(count1);
            denialList2.add(denial3);
        }
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("denials", denialList2);
        return "denial-count";
    }

    @PostMapping("/showFlagCount")
    public String showFlagCount(@RequestParam String startDate, @RequestParam String endDate, Model model){
        List<Flag> flags = flagRepository.getFlagUsingDate(startDate, endDate);
        List<Flag> flagList2 = new ArrayList<>();
        for(Flag flag2 : flags) {
            Flag flag3 = new Flag();
            Long count1 = flagRepository.countGroupByUser(startDate, endDate, flag2.getUser());
            flag3.setUser(flag2.getUser());
            flag3.setCount(count1);
            flagList2.add(flag3);
        }
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("flags", flagList2);
        return "flag-count";
    }

    @GetMapping("/agents")
    public String showAgents(Model model){
        ArrayList<AgentData> agentsData = customerDetailServiceImpl.showAgents();
        model.addAttribute("agents", agentsData);
        return "agents";
    }
}
