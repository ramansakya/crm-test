package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Denial;
import com.cellcom.protocol1.entity.Flag;
import com.cellcom.protocol1.repository.DenialRepository;
import com.cellcom.protocol1.repository.FlagRepository;
import com.cellcom.protocol1.service.CustomerDetailServiceImpl;
import com.cellcom.protocol1.service.SemiKYCServiceImpl;
import com.cellpay.v2.cellpaydatas.entity.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CustomerController {

    @Autowired
    private DenialRepository denialRepository;

    @Autowired
    private FlagRepository flagRepository;

    @Autowired
    private CustomerDetailServiceImpl customerDetailServiceImpl;

    @Autowired
    private SemiKYCServiceImpl semiKYCServiceImpl;

    @GetMapping("/customers")
    public String showList(Model model){
        ArrayList<SemiKYCUsers> semiKYCUsers = semiKYCServiceImpl.showList();
        List<String> index = new ArrayList<>();
        List<Denial> denials = denialRepository.findAll();
        List<Flag> flags = flagRepository.findAll();
        for(Integer i=0; i < semiKYCUsers.size(); i++){
            for (Integer j=0; j < denials.size(); j++) {
                if (semiKYCUsers.get(i).getMemberId().equals(denials.get(j).getMemberId().toString())) {
                    index.add(denials.get(j).getMemberId().toString());
                }
            }
        }
        for(int i=0; i < index.size() ; i++){
            String data = index.get(i);
            semiKYCUsers.removeIf(nSemiKYCUsers -> nSemiKYCUsers.getMemberId().equals(data));
        }
        ArrayList<SemiKYCUsers> newSemiKYCUsers = new ArrayList<>();
        for(SemiKYCUsers sUsers : semiKYCUsers){
            newSemiKYCUsers.add(sUsers);
        }
        for(int i=0; i < newSemiKYCUsers.size(); i++){
            for (int j=0; j < flags.size(); j++) {
                if (newSemiKYCUsers.get(i).getMemberId().equals(flags.get(j).getMemberId().toString())) {
                    newSemiKYCUsers.remove(i);
                }
            }
        }
        ArrayList<SemiKYCUsers> newWithoutFlagSemiKYCUsers = new ArrayList<>();
        for(SemiKYCUsers sUsers : newSemiKYCUsers){
            newWithoutFlagSemiKYCUsers.add(sUsers);
        }
        model.addAttribute("semiKYCUsers", newWithoutFlagSemiKYCUsers);
        return "customers";
    }

    @GetMapping("/latest-customers")
    public String showLatest(Model model){
        ArrayList<CustomerDataLatestModified> customerDataLatestModifieds = customerDetailServiceImpl.showData();
        List<String> index = new ArrayList<>();
        List<Denial> denials = denialRepository.findAll();
        List<Flag> flags = flagRepository.findAll();
        for(Integer i=0; i < customerDataLatestModifieds.size(); i++){
            for (Integer j=0; j < denials.size(); j++) {
                if (customerDataLatestModifieds.get(i).getMemberId().equals(denials.get(j).getMemberId().toString())) {
                    index.add(denials.get(j).getMemberId().toString());
                }
            }
        }
        for(int i=0; i < index.size() ; i++){
            String data = index.get(i);
            customerDataLatestModifieds.removeIf(nSemiKYCUsers -> nSemiKYCUsers.getMemberId().equals(data));
        }
        ArrayList<CustomerDataLatestModified> newSemiKYCUsers = new ArrayList<>();
        for(CustomerDataLatestModified customerDataLatestModified : customerDataLatestModifieds){
            newSemiKYCUsers.add(customerDataLatestModified);
        }
        for(int i=0; i < newSemiKYCUsers.size(); i++){
            for (int j=0; j < flags.size(); j++) {
                if (newSemiKYCUsers.get(i).getMemberId().equals(flags.get(j).getMemberId().toString())) {
                    newSemiKYCUsers.remove(i);
                }
            }
        }
        ArrayList<CustomerDataLatestModified> newWithoutFlagSemiKYCUsers = new ArrayList<>();
        for(CustomerDataLatestModified sUsers : newSemiKYCUsers){
            newWithoutFlagSemiKYCUsers.add(sUsers);
        }
        model.addAttribute("semiKYCUsers", newWithoutFlagSemiKYCUsers);
        return "latest-customers";
    }

    @GetMapping("/customer/{mobile}")
    public String showDetails(@PathVariable String mobile, Model model){
        CustomerDetails customerDetails = customerDetailServiceImpl.showCustomerDetails(mobile);
        ArrayList<CustomerKYCDetails> customerKYCDetails = customerDetailServiceImpl.showCustomerKYCDetails(mobile);
        CustomerStatus customerStatus = customerDetailServiceImpl.showCustomerStatus(mobile);
        ArrayList<CustomerDocuments> customerDocuments = customerDetailServiceImpl.showCustomerDocuments(mobile);
        ArrayList<String> images = new ArrayList<>();
        for(int i=0; i< customerDocuments.size(); i++){
            byte[] imgBytes = customerDocuments.get(i).getDocImage();
            String docs = Base64.encodeBase64String(imgBytes);
            images.add(docs);
        }
        model.addAttribute("customerDetails", customerDetails);
        model.addAttribute("customerKYCDetails", customerKYCDetails);
        model.addAttribute("customerStatus", customerStatus);
        model.addAttribute("customerDocuments", customerDocuments);
        model.addAttribute("images", images);
        return "customer";
    }

    @PostMapping("/customer")
    public String showDetail(@RequestParam String number){
        return "redirect:customer/"+number;
    }

}