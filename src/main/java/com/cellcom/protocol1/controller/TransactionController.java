package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.service.CustomerTransactionServiceImpl;
import com.cellpay.v2.cellpaydatas.entity.CustomerTransactionHistory;
import com.cellpay.v2.cellpaydatas.entity.FailureTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@Controller
public class TransactionController {

    @Autowired
    private CustomerTransactionServiceImpl customerTransactionServiceImpl;

    @GetMapping("/show-fail")
    public String show(){
        return "show-fail";
    }

    @GetMapping("/show-success")
    public String failure(){
        return "show-success";
    }

    @PostMapping(value = "/success")
    public ModelAndView input(@RequestParam("mobile") String mobile, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate){
        ModelAndView modelAndView = new ModelAndView();
        ArrayList<CustomerTransactionHistory> customerTransactionsHistory = customerTransactionServiceImpl.showTransactions(mobile, startDate, endDate);
        modelAndView.addObject("transactions", customerTransactionsHistory);
        modelAndView.setViewName("transaction");
        return modelAndView;
    }

    @PostMapping(value = "/failure")
    public ModelAndView fail(@RequestParam("mobile") String mobile, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate){
        ModelAndView modelAndView = new ModelAndView();
        ArrayList<FailureTransaction> failureTransactions = customerTransactionServiceImpl.failTransactions(mobile, startDate, endDate);
        modelAndView.addObject("transactions", failureTransactions);
        modelAndView.setViewName("failed-transaction");
        return modelAndView;
    }
}
