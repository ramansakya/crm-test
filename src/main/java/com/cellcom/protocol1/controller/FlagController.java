package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Flag;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.FlagRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class FlagController {

    @Autowired
    private FlagRepository flagRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private UserRepository userRepository;

    String email = null;

    @GetMapping("/flag")
    public String flag(Model model){
        List<Flag> flags = flagRepository.findAll();
        model.addAttribute("flags", flags);
        return "flag";
    }

    @GetMapping("/setFlag/{memberId}/{name}/{mobileNumber}")
    public String setFlag(@PathVariable Long memberId, @PathVariable String name, @PathVariable String mobileNumber, @RequestParam String remarks, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        Flag flag = new Flag();
        flag.setMemberId(memberId);
        flag.setUser(user);
        flag.setMemberName(name);
        flag.setMobile(mobileNumber);
        flag.setDate(userServiceImpl.getCurrentDate());
        flag.setDescription(remarks);
        flagRepository.save(flag);
        model.addAttribute("flag", flag);
        return "redirect:/flag?setFlag=success";
    }
}
