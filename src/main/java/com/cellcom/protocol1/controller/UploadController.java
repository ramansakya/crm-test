package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.*;
import com.cellcom.protocol1.repository.ProblemRepository;
import com.cellcom.protocol1.repository.SolutionRepository;
import com.cellcom.protocol1.repository.TicketRepository;
import com.cellcom.protocol1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class UploadController {

    @Autowired
    private SolutionRepository solutionRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProblemRepository problemRepository;

//     @PostMapping("/test")
//     public String home() {
//         String csvFile = "E:/cellpay-test/cellpayprotocol/Daily Call Logs.csv";
//         String line = "";
//         String splitBy = ",";
//         BufferedReader br = null;
//         try {
//             br = new BufferedReader(new FileReader(csvFile));
//             br.readLine();
//             while ((line = br.readLine()) != null) {
//                 Ticket ticket = new Ticket();
//                 Set<TicketProblem> problems1 = new HashSet<>();
//                 Set<TicketSolution> solutions1 = new HashSet<>();
//                 String[] field = line.split(splitBy);
//                 List<User> users = userRepository.findAll();
//                 for(User user: users) {
//                     if (field[5].equalsIgnoreCase(user.getName())) {
//                         ticket.setUser(user);
// //                        ticket.setCreationDate(field[0]);
//                         ticket.setCustomerName(field[1]);
//                         ticket.setCustomerMobile(field[2]);
//                         ticket.setAddress(field[6]);
//                         ticket.setRemarks(field[8]);
//                         ticket.setStatus(field[7]);
//                         List<Problem> problems = problemRepository.findAll();
//                         for(Problem problem: problems) {
//                             TicketProblem ticketProblem = null;
//                             if (field[4].equalsIgnoreCase(problem.getName())) {
//                                 ticketProblem = new TicketProblem();
//                                 ticketProblem.setProblem(field[4]);
//                                 ticketProblem.setTicket(ticket);
//                                 problems1.add(ticketProblem);
//                             }
//                         }
//                         ticket.setProblems(problems1);

//                         List<Solution> solutions = solutionRepository.findAll();
//                         for(Solution solution: solutions) {
//                             TicketSolution ticketSolution = null;
//                             if (field[3].equalsIgnoreCase(solution.getName())) {
//                                 ticketSolution = new TicketSolution();
//                                 ticketSolution.setSolution(field[3]);
//                                 ticketSolution.setTicket(ticket);
//                                 solutions1.add(ticketSolution);
//                             }
//                         }
//                         ticket.setSolutions(solutions1);
//                         ticketRepository.save(ticket);
//                     }
//                 }
//             }
//         } catch (FileNotFoundException e) {
//             e.printStackTrace();
//         } catch (IOException e) {
//             e.printStackTrace();
//         }
//         return "tickets";
//     }
}
