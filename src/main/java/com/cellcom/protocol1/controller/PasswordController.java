package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.SendEmail;
import com.cellcom.protocol1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import java.io.IOException;

@Controller
public class PasswordController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private SendEmail sendEmail;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    String email = null;

    @GetMapping("/change-password")
    public ModelAndView changePasswordForm(){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        modelAndView.addObject("user", user);
        modelAndView.setViewName("change-password");
        return modelAndView;
    }

    @PostMapping("/firstTimePassword")
    public ModelAndView changePassword(@RequestParam("password") String password){
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        user.setEmail(email);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setFirstLogin(true);
        userRepository.save(user);
        modelAndView.addObject("message", "Your Password was updated Successfully");
        modelAndView.setViewName("redirect:/welcome?success=true");
        return modelAndView;
    }

    @GetMapping("/message")
    public String message(){ return "message";}

    @GetMapping("/recover-password/{email}/{token}")
    public String changeForm(@PathVariable String email, @PathVariable String token, Model model) {
        User user = userRepository.findByEmail(email);
        user.setEmail(email);
        user.setToken(token);
        userRepository.save(user);
        model.addAttribute("user", user);
        return "recover-password";
    }

    @PostMapping("/sendToken")
    public String recoverPassword(@RequestParam String mail, Model model) throws IOException, MessagingException {
        String token;
        User user = userRepository.findByEmail(mail);
        model.addAttribute("user", user);
        token = userServiceImpl.getRandomString();
        String body= "Please Click the Link to change your password. \n" + "<a href=http://192.168.0.131:8081/recover-password/" + user.getEmail() + "/" + token + ">click here</a>. \n ";
        if(userServiceImpl.isUserAlreadyPresent(user)) {
            sendEmail.sendEmail(body, user.getEmail());
            model.addAttribute("message", "Please check your mail.");
        }
        else {
            model.addAttribute("message", "The email address doesn't exist in our database.");
        }
        return "message";
    }

    @PostMapping("/password")
    public String change(@RequestParam String email, @RequestParam("password") String password, Model model){
        User user = userRepository.findByEmail(email);
        System.out.println(user);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(user);
        model.addAttribute("user", user);
        return "redirect:/login?password-change=success";
    }
}
