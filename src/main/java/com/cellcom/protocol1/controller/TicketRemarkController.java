package com.cellcom.protocol1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cellcom.protocol1.entity.Ticket;
import com.cellcom.protocol1.entity.TicketComments;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.TicketCommentsRepository;
import com.cellcom.protocol1.repository.TicketRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.UserServiceImpl;

@Controller
public class TicketRemarkController {

    @Autowired
    private TicketCommentsRepository ticketCommentsRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;


    @GetMapping("/comments/{ticket_id}")
    public String viewComments(@PathVariable String ticket_id, Model model) {
        Ticket ticket = ticketRepository.findById(Long.parseLong(ticket_id)).get();
        List<TicketComments> comments = ticketCommentsRepository.findByTicketId(ticket);
        List<User> users = userRepository.findAll();
        model.addAttribute("ticket", ticket);
        model.addAttribute("users", users);
        model.addAttribute("comments", comments);
        return "ticket-remarks";
    }

    @GetMapping("/addPriority/{ticket_id}/{priority}")
    public String addPriority(@PathVariable String ticket_id, @PathVariable String priority) {
        String email = null;
        Ticket ticket = ticketRepository.findById(Long.parseLong(ticket_id)).get();
        ticket.setPriority(priority);

        ticketRepository.save(ticket);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }

        User user = userRepository.findByEmail(email);
        String description = user.getName() + " has changed the priority to " + priority;
        TicketComments comment = new TicketComments();
        comment.setCreation_date(userServiceImpl.getTodayDate());
        comment.setDescription(description);
        comment.setSystemComment(true);
        comment.setTicket(ticket);
        comment.setUser(user);
        ticketCommentsRepository.save(comment);
        return "redirect:/comments/" + ticket_id;
    }

    @GetMapping("/markComplete/{ticket_id}")
    public String markComplete(@PathVariable String ticket_id) {
        String email = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }

        User user = userRepository.findByEmail(email);
        Ticket ticket = ticketRepository.findById(Long.parseLong(ticket_id)).get();
        String description = user.getName() + " has completed this task.";
        TicketComments comment = new TicketComments();
        comment.setCreation_date((userServiceImpl.getTodayDate()));
        comment.setDescription(description);
        comment.setSystemComment(true);
        comment.setTicket(ticket);
        comment.setUser(user);
        ticketCommentsRepository.save(comment);
        ticket.setStatus("Resolved");
        ticketRepository.save(ticket);
        return "redirect:/comments/" + ticket_id;
    }

    @GetMapping("/ticketAssign/{assign_user}/{ticket_id}")
    public String assginTicket(@PathVariable String assign_user, @PathVariable String ticket_id) {
        String email = null;
        Ticket ticket = ticketRepository.findById(Long.parseLong(ticket_id)).get();
        User assigned_user = userRepository.findById(Long.parseLong(assign_user)).get();
        ticket.setAssignedUser(assigned_user);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        ticketRepository.save(ticket);
        String description = user.getName() + " has assigned this task to " + assigned_user.getName() + ".";
        TicketComments ticketComment = new TicketComments();
        ticketComment.setCreation_date(userServiceImpl.getTodayDate());
        ticketComment.setDescription(description);
        ticketComment.setSystemComment(true);
        ticketComment.setTicket(ticket);
        ticketComment.setUser(user);
        ticketCommentsRepository.save(ticketComment);
        return "redirect:/comments/" + ticket_id;
    }

    @PostMapping("/ticketAssign")
    public String ticketAssign(@RequestParam String assigned_user, @RequestParam String ticket_id) {
        Ticket ticket = ticketRepository.findById(Long.parseLong(ticket_id)).get();
        User user = userRepository.findById(Long.parseLong(assigned_user)).get();
        ticket.setAssignedUser(user);
        ticketRepository.save(ticket);
        return "redirect:/comments/" + ticket_id;
    }

    @PostMapping("/comments")
    public String addComments(@RequestParam String description, @RequestParam String ticket_id) {
        String email = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        TicketComments ticketComment = new TicketComments();
        User user = userRepository.findByEmail(email);
        Ticket ticket = ticketRepository.findById(Long.parseLong(ticket_id)).get();
        ticketComment.setUser(user);
        ticketComment.setDescription(description);
        ticketComment.setTicket(ticket);
        ticketComment.setCreation_date(userServiceImpl.getTodayDate());
        ticketCommentsRepository.save(ticketComment);
        return "redirect:/comments/" + ticket_id;
    }
}
