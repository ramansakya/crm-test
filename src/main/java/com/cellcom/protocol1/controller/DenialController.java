package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Denial;
import com.cellcom.protocol1.entity.Flag;
import com.cellcom.protocol1.entity.Member;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.DenialRepository;
import com.cellcom.protocol1.repository.FlagRepository;
import com.cellcom.protocol1.repository.MemberRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.CustomerDetailServiceImpl;
import com.cellcom.protocol1.service.UserServiceImpl;
import com.cellpay.v2.cellpaydatas.entity.*;
import com.cellpay.v2.cellpaydatas.serviceImpl.CustomerDetailsServiceImpl;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DenialController {

    @Autowired
    private FlagRepository flagRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private DenialRepository denialRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerDetailServiceImpl customerDetailServiceImpl;

    String email = null;

    @GetMapping("/deny")
    public String deny(Model model) {
        List<Denial> denials = denialRepository.findAll();
        List<Member> members = memberRepository.findAll();
        List<Flag> flags = flagRepository.findAll();
        List<Long> index = new ArrayList<>();
        List<Long> index1 = new ArrayList<>();
        for(int i=0; i < denials.size(); i++){
            for (int j=0; j < members.size(); j++) {
                if (denials.get(i).getMemberId().equals(members.get(j).getId())) {
                    index.add(members.get(j).getId());
                }
            }
        }
        for(int i=0; i < index.size() ; i++) {
            Long data = index.get(i);
            denials.removeIf(nDenial -> nDenial.getMemberId().equals(data));
        }
        ArrayList<Denial> newDenials = new ArrayList<>();
        for(Denial denial : denials){
            newDenials.add(denial);
        }
        for(int i=0; i < newDenials.size(); i++){
            for (int j=0; j < flags.size(); j++) {
                if (newDenials.get(i).getMemberId().equals(flags.get(j).getMemberId())) {
                    index1.add(flags.get(j).getMemberId());
                }
            }
        }
        for(int i=0; i < index1.size() ; i++) {
            Long data1 = index1.get(i);
            newDenials.removeIf(nDenial -> nDenial.getMemberId().equals(data1));
        }
        ArrayList<Denial> newWithoutFlagAndApprove = new ArrayList<>();
        for(Denial nDenial : newDenials){
            newWithoutFlagAndApprove.add(nDenial);
        }
        model.addAttribute("denials", newWithoutFlagAndApprove);
        return "deny";
    }

    @GetMapping("/deniedCustomer/{mobile}")
    public String viewDetails(@PathVariable String mobile, Model model){
        CustomerDetails customerDetails = customerDetailServiceImpl.showCustomerDetails(mobile);
        ArrayList<CustomerKYCDetails> customerKYCDetails = customerDetailServiceImpl.showCustomerKYCDetails(mobile);
        CustomerStatus customerStatus = customerDetailServiceImpl.showCustomerStatus(mobile);
        ArrayList<CustomerDocuments> customerDocuments = customerDetailServiceImpl.showCustomerDocuments(mobile);
        ArrayList<String> images = new ArrayList<>();
        for(int i=0; i< customerDocuments.size(); i++){
            byte[] imgBytes = customerDocuments.get(i).getDocImage();
            String docs = Base64.encodeBase64String(imgBytes);
            images.add(docs);
        }
        model.addAttribute("customerDetails", customerDetails);
        model.addAttribute("customerKYCDetails", customerKYCDetails);
        model.addAttribute("customerStatus", customerStatus);
        model.addAttribute("customerDocuments", customerDocuments);
        model.addAttribute("images", images);
        return "customer";
    }

    @GetMapping("/deny/{memberId}/{group}")
    public String remarks(@PathVariable Long memberId, @PathVariable String group, @RequestParam String description, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        Denial denial = new Denial();
        denial.setUser(user);
        denial.setMemberId(memberId);
        MemberInfo memberInfo = CustomerDetailsServiceImpl.getMemberInfo(memberId.toString());
        denial.setName(memberInfo.getMemberName());
        denial.setMobile(memberInfo.getMobileNumber());
        denial.setDate(userServiceImpl.getCurrentDate());
        denial.setDescription(description);
        denial.setGroup(group);
        denialRepository.save(denial);
        model.addAttribute("denial", denial);
        return "redirect:/customers?denied=success";
    }
}
