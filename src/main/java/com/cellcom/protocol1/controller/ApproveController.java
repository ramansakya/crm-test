package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Member;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.MemberRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.UserServiceImpl;
import com.cellpay.v2.cellpaydatas.control.ApproveMember;
import com.cellpay.v2.cellpaydatas.entity.ApprovalData;
import com.cellpay.v2.cellpaydatas.entity.MemberInfo;
import com.cellpay.v2.cellpaydatas.serviceImpl.CustomerDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

@Controller
public class ApproveController {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    String email = null;

    @GetMapping("/approve")
    public String showApprovals(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        List<Member> members = memberRepository.findAll();
        model.addAttribute("members", members);
        model.addAttribute("user", user);
        return "approve";
    }

    @GetMapping("/approve/{memberId}/{groupId}")
    public String approve(@PathVariable Long memberId, @PathVariable Long groupId, Model model) throws InterruptedException {
        String toGroup = "";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        if(String.valueOf(groupId).equalsIgnoreCase("16")){
            toGroup = "Full KYC Customer";
        } else if(String.valueOf(groupId).equalsIgnoreCase("22")){
            toGroup = "Cellpay Agents";
        } else if(String.valueOf(groupId).equalsIgnoreCase("20")){
            toGroup = "OTC Merchant";
        }
        ApprovalData approvalData = new ApprovalData();
        approvalData.setLoginUser("supportadmin");
        approvalData.setLoginPassword("karanjit@123");
        approvalData.setMemberId(String.valueOf(memberId));
        approvalData.setToGroup(toGroup);
        approvalData.setCurrentUser(email);

        boolean status = ApproveMember.approveMember(approvalData);
        if(status) {
            Member member = new Member();
            User user = userRepository.findByEmail(email);
            member.setUser(user);
            member.setId(Long.valueOf(memberId));
           member.setDate(userServiceImpl.getTodayDate());
            MemberInfo memberInfo = CustomerDetailsServiceImpl.getMemberInfo(memberId.toString());
            member.setName(memberInfo.getMemberName());
            member.setMobile(memberInfo.getMobileNumber());
            member.setGroup(toGroup);
            memberRepository.save(member);
            model.addAttribute("member", member);
            List<Member> members = memberRepository.findAll();
            model.addAttribute("members", members);
            return "redirect:/latest-customers?approval=success";
        } else {
            model.addAttribute("status", "failed");
            return "approve";
        }
    }
}
