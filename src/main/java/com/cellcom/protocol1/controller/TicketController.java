package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.*;
import com.cellcom.protocol1.repository.*;
import com.cellcom.protocol1.service.UserServiceImpl;
import org.eclipse.jetty.util.DateCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class TicketController {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private ProblemRepository problemRepository;

    @Autowired
    private SolutionRepository solutionRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    String email = null;

    @GetMapping("/ticket")
    public String ticket(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        Ticket ticket = new Ticket();
        List<Department> departments = departmentRepository.findAll();
        User user = userRepository.findByEmail(email);
        List<Problem> problems = problemRepository.findAll();
        model.addAttribute("problems", problems);
        List<Solution> solutions = solutionRepository.findAll();
        model.addAttribute("solutions", solutions);
        ArrayList<String> statusList = new ArrayList<>();
        statusList.add("Pending");
//        statusList.add("Progress");
        statusList.add("Resolved");
        model.addAttribute("statusList", statusList);
        model.addAttribute("ticket", ticket);
        model.addAttribute("user", user);
        model.addAttribute("departments", departments);
        return "ticket";
    }

    @GetMapping("/tickets")
    public String showTicket(Model model){
        List<Ticket> tickets = ticketRepository.findAll();
        model.addAttribute("tickets", tickets);
        return "tickets";
    }

    @PostMapping("/openTicket")
    public String open(@RequestParam String customerName, @RequestParam String customerMobile,
                       @RequestParam String address, @RequestParam(value = "probs" , required = false) String[] probs,
                       @RequestParam(value = "sols", required = false) String[] sols, @RequestParam String remarks, @RequestParam String status) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        Ticket ticket = new Ticket();
        ticket.setUser(user);
        ticket.setCustomerName(customerName);
        ticket.setCustomerMobile(customerMobile);
        ticket.setAddress(address);
        Set<TicketProblem> problems = new HashSet<>();
        if(probs != null){
            TicketProblem problem1 = null;
            for (int i=0; i< probs.length; i++){
                problem1 = new TicketProblem();
                problem1.setProblem(probs[i]);
                problems.add(problem1);
                problem1.setTicket(ticket);
            }
        }
//        else {
//            TicketProblem problem2 = new TicketProblem();
//            Problem problem3 = new Problem();
//            problem3.setName(problem);
//            problemRepository.save(problem3);
//            problem2.setProblem(problem);
//            problem2.setTicket(ticket);
//            problems.add(problem2);
//        }
        ticket.setProblems(problems);
        Set<TicketSolution> solutions = new HashSet<>();
        if(sols != null){
            TicketSolution solution1 = null;
            for(int i=0; i< sols.length ; i++){
                solution1 = new TicketSolution();
                solution1.setSolution(sols[i]);
                solution1.setTicket(ticket);
                solutions.add(solution1);
            }
        }
//        else {
//            TicketSolution solution2 = new TicketSolution();
//            Solution solution3 = new Solution();
//            solution3.setName(solution);
//            solutionRepository.save(solution3);
//            solution2.setSolution(solution);
//            solution2.setTicket(ticket);
//            solutions.add(solution2);
//        }
        ticket.setSolutions(solutions);
        ticket.setRemarks(remarks);
        ticket.setStatus(status);
        ticket.setCreationDate(userServiceImpl.getTodayDate());
        ticketRepository.save(ticket);
        return "redirect:/tickets";
    }

    @GetMapping("/viewTickets")
    public String viewTickets(Model model) {
        List<Ticket> tickets = ticketRepository.findAll();
        model.addAttribute("tickets",tickets);
        return "view-tickets";
    }
}
