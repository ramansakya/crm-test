package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Department;
import com.cellcom.protocol1.entity.Role;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.entity.UserProfile;
import com.cellcom.protocol1.repository.DepartmentRepository;
import com.cellcom.protocol1.repository.RoleRepository;
import com.cellcom.protocol1.repository.UserProfileRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.SendEmail;
import com.cellcom.protocol1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SendEmail sendEmail;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @GetMapping("/users")
    public String adminHome(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "users";
    }

    @GetMapping("/assign-role")
    public String assignRole(Model model){
        model.addAttribute("roles", roleRepository.findAll());
        return "assign-role";
    }

    @GetMapping("/assign-role/{id}/register")
    public String showForm(@PathVariable Long id, Model model){
        User user = new User();
        List<Department> departments = departmentRepository.findAll();
        Role role = roleRepository.findById(id).get();
        user.setRole(role);
        model.addAttribute("role", role);
        model.addAttribute("user", user);
        model.addAttribute("departments", departments);
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid User user, @RequestParam String depart, BindingResult bindingResult, Model model) throws IOException, MessagingException {
        String password = null;
        if(bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
        }
        else if(userServiceImpl.isUserAlreadyPresent(user)){
            model.addAttribute("successMessage", "user already exists!");
        }
        else {
            password = userServiceImpl.getRandomString();
            user.setPassword(password);
            user.setStatus(false);
            Department departmentObj = departmentRepository.findById(Long.parseLong(depart)).get();
            user.setDepartment(departmentObj);
            userServiceImpl.saveUser(user);
            UserProfile userProfile = new UserProfile();
            userProfile.setUser(user);
            userProfile.setJobTitle("");
            userProfile.setNotes("");
            userProfile.setJoinDate("");
            userProfile.setEmployer("Cellcom Private Limited");
            userProfileRepository.save(userProfile);
            model.addAttribute("successMessage", "User is registered successfully!");
        }
        model.addAttribute("user", user);
        String body= "Please Click the Link to verify your account. \n" + "Your password is: " + password +
                ". \n" + "<a href=http://192.168.0.131:8081/verify/" +user.getEmail() +">click here</a>.";
        sendEmail.sendEmail(body, user.getEmail());
        return "redirect:users?register=success";
    }

    @GetMapping("/verify/{email}")
    public String sendEmail(@PathVariable("email") String email, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        user.setStatus(true);
        userRepository.save(user);
        model.addAttribute("user", user);
        return "verify";
    }
}