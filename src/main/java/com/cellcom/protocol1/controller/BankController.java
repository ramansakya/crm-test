package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Bank;
import com.cellcom.protocol1.entity.BankComment;
import com.cellcom.protocol1.entity.PointOfContact;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.BankCommentRepository;
import com.cellcom.protocol1.repository.BankRepository;
import com.cellcom.protocol1.repository.PointOfContactRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BankController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PointOfContactRepository pointOfContactRepository;

    @Autowired
    private BankCommentRepository bankCommentRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private BankRepository bankRepository;

    String email = null;

    @GetMapping("/bank")
    public String bank(){
        return "add-bank";
    }

    @GetMapping("/viewBanks")
    public String viewBanks(Model model){
        List<Bank> banks = bankRepository.findAll();
        model.addAttribute("banks", banks);
        return "view-banks";
    }

    @GetMapping("/viewBankDetails/{bank_id}")
    public String viewBankDetails(@PathVariable String bank_id, Model model){
        Bank bank = bankRepository.findById(Long.valueOf(bank_id)).get();
        List<PointOfContact> pointOfContacts = pointOfContactRepository.findAllByBank(bank);
        model.addAttribute("pointOfContacts", pointOfContacts);
        model.addAttribute("bank", bank);
        return "bank-details";
    }

    @PostMapping("/addBank")
    public String addBank(@RequestParam String name, @RequestParam String test_ip, @RequestParam String test_port, @RequestParam String live_ip,
                          @RequestParam String live_port, @RequestParam String cbs_test_ip, @RequestParam String cbs_test_port, @RequestParam String cbs_live_ip,
                          @RequestParam String cbs_live_port, @RequestParam String followUp, @RequestParam String vpnParameters, @RequestParam String vpn,
                          @RequestParam String isoTesting, @RequestParam String accountLinkMethod, @RequestParam String accountLink, @RequestParam String testing,
                          @RequestParam String mgmtTeamApproval, @RequestParam String live){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        Bank bank = new Bank();
        bank.setName(name);
        bank.setTestIP(test_ip);
        bank.setTestPort(test_port);
        bank.setLiveIP(live_ip);
        bank.setLivePort(live_port);
        bank.setCbsTestIP(cbs_test_ip);
        bank.setCbsTestPort(cbs_test_port);
        bank.setCbsLiveIP(cbs_live_ip);
        bank.setCbsLivePort(cbs_live_port);
        bank.setFollowUp(followUp);
        bank.setVpnParameters(vpnParameters);
        bank.setVpn(vpn);
        bank.setIsoTesting(isoTesting);
        bank.setVpnParameters(vpnParameters);
        bank.setVpn(vpn);
        bank.setIsoTesting(isoTesting);
        bank.setAccountLinkMethod(accountLinkMethod);
        bank.setAccountLink(accountLink);
        bank.setTesting(testing);
        bank.setManagementTeamApproval(mgmtTeamApproval);
        bank.setLive(live);
        bank.setUser(user);
        bankRepository.save(bank);
        return "redirect:/viewBanks?bankAdd=success";
    }

    @GetMapping("/editBank/{bank_id}")
    public String editBank(@PathVariable String bank_id, Model model) {
        Bank bank = bankRepository.findById(Long.valueOf(bank_id)).get();
        List<BankComment> bankComments = bankCommentRepository.findAllByBank(bank);
        model.addAttribute("comments", bankComments);
        model.addAttribute("bank", bank);
        return "edit-bank";
    }

    @PostMapping("/updateBank")
    public String updateBank(@RequestParam String bank_id, @RequestParam String name, @RequestParam String test_ip, @RequestParam String test_port,
                             @RequestParam String live_ip, @RequestParam String live_port, @RequestParam String cbs_test_ip, @RequestParam String cbs_test_port,
                             @RequestParam String cbs_live_ip, @RequestParam String cbs_live_port, @RequestParam String followUp,
                             @RequestParam String vpnParameters, @RequestParam String vpn, @RequestParam String isoTesting,
                             @RequestParam String accountLinkMethod, @RequestParam String accountLink, @RequestParam String testing,
                             @RequestParam String mgmtTeamApproval, @RequestParam String live){

        Bank bank = bankRepository.findById(Long.valueOf(bank_id)).get();
        bank.setName(name);
        bank.setTestIP(test_ip);
        bank.setTestPort(test_port);
        bank.setLiveIP(live_ip);
        bank.setLivePort(live_port);
        bank.setCbsTestIP(cbs_test_ip);
        bank.setCbsTestPort(cbs_test_port);
        bank.setCbsLiveIP(cbs_live_ip);
        bank.setCbsLivePort(cbs_live_port);
        bank.setFollowUp(followUp);
        bank.setVpnParameters(vpnParameters);
        bank.setVpn(vpn);
        bank.setIsoTesting(isoTesting);
        bank.setVpnParameters(vpnParameters);
        bank.setVpn(vpn);
        bank.setIsoTesting(isoTesting);
        bank.setAccountLinkMethod(accountLinkMethod);
        bank.setAccountLink(accountLink);
        bank.setTesting(testing);
        bank.setManagementTeamApproval(mgmtTeamApproval);
        bank.setLive(live);
        bankRepository.save(bank);
        return "redirect:/editBank/"+bank_id;
    }

    @PostMapping("/contact")
    public String contact(@RequestParam String bankId, @RequestParam String nam, @RequestParam String number, @RequestParam String mobile, @RequestParam String extension,
                          @RequestParam String email){
        Bank bank = bankRepository.findById(Long.valueOf(bankId)).get();
        PointOfContact pointOfContact = new PointOfContact();
        pointOfContact.setBank(bank);
        pointOfContact.setName(nam);
        pointOfContact.setNumber(number);
        pointOfContact.setMobile(mobile);
        pointOfContact.setExtension(extension);
        pointOfContact.setEmail(email);
        pointOfContactRepository.save(pointOfContact);
        return "redirect:/editBank/"+bankId;
    }

    @PostMapping("/description")
    public String remark(@RequestParam String description, @RequestParam String baink_id){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        Bank bank = bankRepository.findById(Long.valueOf(baink_id)).get();
        BankComment bankComment = new BankComment();
        bankComment.setUser(user);
        bankComment.setBank(bank);
        bankComment.setComment(description);
        bankComment.setCreation_date(userServiceImpl.getTodayDate());
        bankCommentRepository.save(bankComment);
        return "redirect:/editBank/"+baink_id;
    }
}
