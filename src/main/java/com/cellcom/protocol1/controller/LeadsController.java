package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Lead;
import com.cellcom.protocol1.entity.Operator;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.repository.LeadRepository;
import com.cellcom.protocol1.repository.OperatorRepository;
import com.cellcom.protocol1.repository.UserRepository;
import com.cellcom.protocol1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LeadsController {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private UserRepository userRepository;

    String email = null;

    @GetMapping("/view-leads")
    public String showLeads(Model model){
        List<Lead> leads = leadRepository.findAll();
        model.addAttribute("leads", leads);
        return "view-leads";
    }

    @GetMapping("/lead")
    public String leads(Model model){
        ArrayList<String> statusList = new ArrayList<>();
        statusList.add("Pending");
        statusList.add("Progress");
        statusList.add("Done");
        model.addAttribute("statusList", statusList);
        model.addAttribute("lead", new Lead());
        return "lead";
    }

    @PostMapping("/addLead")
    public String addLeads(@Valid Lead lead, @RequestParam String status, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        lead.setUser(user);
        lead.setDate(userServiceImpl.getCurrentDate());
        lead.setStatus(status);
        leadRepository.save(lead);
        model.addAttribute("lead", lead);
        List<Lead> leads = leadRepository.findAll();
        model.addAttribute("leads", leads);
        return "redirect:/view-leads?leads=added";
    }

    @GetMapping("/leads-info/{id}")
    public String specificLeads(@PathVariable Long id, Model model){
        Lead lead = leadRepository.findById(id).get();
        List<Operator> operators = operatorRepository.findAllByLead(lead);
        model.addAttribute("operators", operators);
        model.addAttribute("lead", lead);
        return "leads-info";
    }

    @GetMapping("/edit-lead/{id}")
    public String showLead(@PathVariable Long id, Model model){
        Lead lead = leadRepository.findById(id).get();
        ArrayList<String> statusList = new ArrayList<>();
        statusList.add("Pending");
        statusList.add("Progress");
        statusList.add("Done");
        model.addAttribute("statusList", statusList);
        model.addAttribute("lead", lead);
        return "edit-lead";
    }

    @GetMapping("edit-operator/{id}")
    public String editOperator(@PathVariable Long id, Model model){
        Operator operator = operatorRepository.findById(id).get();
        model.addAttribute("operator", operator);
        return "edit-operator";
    }

    @PostMapping("/update-lead")
    public String editLeads(@RequestParam Long id, @RequestParam String cName, @RequestParam String cNumber, @RequestParam String cAddress,
                            @RequestParam String cEmail, @RequestParam String regMobile, @RequestParam String cRNumber, @RequestParam String regBank,
                            @RequestParam String accNumber, @RequestParam String panVat, @RequestParam String rName, @RequestParam String rNumber,
                            @RequestParam String rEmail, @RequestParam String remarks, @RequestParam String status){
        Lead lead = leadRepository.findById(id).get();
        lead.setCompanyName(cName);
        lead.setCompanyNumber(cNumber);
        lead.setCompanyAddress(cAddress);
        lead.setCompanyEmail(cEmail);
        lead.setRegisteredMobile(regMobile);
        lead.setRegistrationNumber(cRNumber);
        lead.setBank(regBank);
        lead.setBankAccount(accNumber);
        lead.setPanVat(panVat);
        lead.setRepresentativeName(rName);
        lead.setRepresentativeNumber(rNumber);
        lead.setRepresentativeEmail(rEmail);
        lead.setRemarks(remarks);
        lead.setStatus(status);
        leadRepository.save(lead);
        return "redirect:/view-leads?update=success";
    }

    @PostMapping("/operator")
    public String operator(@RequestParam String leadId, @RequestParam String nam, @RequestParam String mobile, @RequestParam String branch){
        Lead lead = leadRepository.findById(Long.valueOf(leadId)).get();
        Operator operator = new Operator();
        operator.setLead(lead);
        operator.setName(nam);
        operator.setNumber(mobile);
        operator.setBranch(branch);
        operatorRepository.save(operator);
        return "redirect:/edit-lead/"+leadId;
    }

    @PostMapping("update-operator")
    public String updateOperator(@RequestParam String operatorId, @RequestParam String name, @RequestParam String mobile, @RequestParam String branch,
                                 @RequestParam String leadId){
        Operator operator = operatorRepository.findById(Long.valueOf(operatorId)).get();
        operator.setName(name);
        operator.setNumber(mobile);
        operator.setBranch(branch);
        operatorRepository.save(operator);
        return "redirect:/leads-ino/"+leadId;
    }
}
