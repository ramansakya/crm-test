package com.cellcom.protocol1.controller;

import com.cellcom.protocol1.entity.Ticket;
import com.cellcom.protocol1.entity.User;
import com.cellcom.protocol1.entity.UserProfile;
import com.cellcom.protocol1.repository.*;
import com.cellcom.protocol1.service.DashBoardDataServiceImpl;
import com.cellpay.v2.cellpaydatas.entity.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class  HomeController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private DenialRepository denialRepository;

    @Autowired
    private FlagRepository flagRepository;

    @Autowired
    private DashBoardDataServiceImpl dashBoardDataServiceImpl;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @GetMapping({"/welcome", "/"})
    public String greeting(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = null;
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        if(user.getRole().getName().equals("ROLE_SUPPORT_USER")){
            model.addAttribute("welcomeMessage", "Welcome User!");
        }else{
            model.addAttribute("welcomeMessage", "Welcome Admin!");
        }
        model.addAttribute("user", user);
        if (user.getFirstLogin().equals(false)){
            return "change-password";
        }else{
            Long approvalCount = memberRepository.countAll();
            model.addAttribute("approvalCount", approvalCount);
            Long denialCount = denialRepository.countAll();
            model.addAttribute("denialCount", denialCount);
            Long flagCount = flagRepository.countAll();
            model.addAttribute("flagCount", flagCount);
            DashboardData dashboardData = dashBoardDataServiceImpl.showData();
            model.addAttribute("totalRegistration", dashboardData.getTotalRegistration());
            model.addAttribute("fullKYC", dashboardData.getFullKYC());
            model.addAttribute("semiKYC", dashboardData.getSemiKYC());
            model.addAttribute("totalBank", dashboardData.getTotalBank());
            model.addAttribute("totalMerchant", dashboardData.getTotalMerchant());
            model.addAttribute("semiMerchant", dashboardData.getSemiMerchant());
            model.addAttribute("totalCustomer", dashboardData.getTotalCustomer());
            model.addAttribute("semiCustomer", dashboardData.getSemiCustomer());
            model.addAttribute("totalAgent", dashboardData.getTotalAgent());
            model.addAttribute("semiAgent", dashboardData.getSemiAgent());
            model.addAttribute("monthlyRegistration", dashboardData.getMonthlyRegistration());
            model.addAttribute("weeklyRegistration", dashboardData.getWeeklyRegistration());
            List<Ticket> tickets = ticketRepository.findTicketsByUser(user);
            model.addAttribute("tickets", tickets);
            return "welcome";
        }
    }

    @GetMapping("/userProfile")
    public String viewProfile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = null;
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);

        UserProfile userProfile = userProfileRepository.findByUserId(user);
        System.out.println("JOB TITLE : " + userProfile.getJobTitle());
        if(userProfile != null) {
            model.addAttribute("profile", userProfile);


        }
        String image;
        // System.out.println(userProfile.getUserImage());
        if(userProfile.getUserImage() == null) {
            image = "/images/user.jpg";
        } else {

            image = "data:image/jpg;base64," + Base64.encodeBase64String(userProfile.getUserImage());
        }
        //String image = new ByteArrayResource(userProfile.getUserImage());
        System.out.println(image);
        model.addAttribute("profile", userProfile);
        model.addAttribute("image", image);
        model.addAttribute("user", user);

        return "user-profile";
    }

    @PostMapping("/uploadImage")
    public String uploadImage(@RequestParam("file") MultipartFile file, Model model) throws Exception {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = null;
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // DBFile dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());

            UserProfile userProfile= userProfileRepository.findByUserId(user);
            userProfile.setUserImage(file.getBytes());
            userProfile.setUser(user);
            userProfileRepository.save(userProfile);
        } catch (IOException ex) {
            throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
        }
        UserProfile userProfile= userProfileRepository.findByUserId(user);
        String image = "data:image/jpg;base64," + Base64.encodeBase64String(userProfile.getUserImage());
        model.addAttribute("profile", userProfile);
        model.addAttribute("image", image);
        model.addAttribute("user", user);
        return "user-profile";
    }

    @PostMapping("/updateProfile")
    public String updateProfile(@Valid UserProfile userProfile, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = null;
        if(!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            email = userDetail.getUsername();
        }
        User user = userRepository.findByEmail(email);
        UserProfile profile = userProfileRepository.findByUserId(user);
        profile.setJobTitle(userProfile.getJobTitle());
        profile.setJoinDate(userProfile.getJoinDate());
        profile.setNotes(userProfile.getNotes());
        profile.setEmployer(userProfile.getEmployer());
        userProfileRepository.save(profile);

        return "redirect:/userProfile/";
    }

    @GetMapping("/bankAccountData")
    public String showBankAccountData(Model model){
        List<BankAccountData> bankAccountData = dashBoardDataServiceImpl.showBankAccountData();
        model.addAttribute("bankAccountData", bankAccountData);
        List<NoBankAccountData> noBankAccountData = dashBoardDataServiceImpl.showNoBankAccountData();
        model.addAttribute("noBankAccountData", noBankAccountData);
        return "bank-account";
    }

    @GetMapping("/banks")
    public String showBanks(Model model){
        List<Bank> banks = dashBoardDataServiceImpl.showBanks();
        model.addAttribute("banks", banks);
        return "banks";
    }

    @GetMapping("/merchants")
    public String showMerchants(Model model){
        List<MerchantData> merchants = dashBoardDataServiceImpl.showMerchants();
        model.addAttribute("merchants", merchants);
        return "merchants";
    }
}

